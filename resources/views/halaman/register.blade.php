@extends('layout.master')

@section('judul')
	Register
@endsection

@section('content')
<h1>Buat Account Baru!</h1>
<h4>Sign Up Form</h4>
<form action="/welcome" method="GET">
	@csrf
	<label>First name :</label><br>
	<input type="text" name="namaAwal"><br>
	<label>Last name :</label><br>
	<input type="text" name="namaAkhir"><br><br>

	<label>Gender :</label><br>
	<input type="radio" name="gender">Male <br>
	<input type="radio" name="gender">Female <br>
	<input type="radio" name="gender">Other <br><br>

	<label>Nationality :</label><br>
	<select name="nationality">
		<option>Indonesia</option>
		<option>Amerika</option>
		<option>Inggris</option>
	</select>
	<br><br>
	<label>Language Spoken</label><br>
	<input type="checkbox" name="spoken">Bahasa Indonesia <br>
	<input type="checkbox" name="spoken">English <br>
	<input type="checkbox" name="spoken">Other <br><br>

	<label>Bio :</label><br>
	<textarea cols="26" rows="10" name="bio"></textarea>
	<br>
	<input type="submit" value="Sign Up">
</form>
@endsection
