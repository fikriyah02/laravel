@extends('layout.master')

@section('judul')
	Edit Cast {{$cast->nama}}
@endsection

@section('content')
<form action="/cast/{{$cast->id}}" method="POST">
    @method('PUT')
    @csrf
		<div class="form-group">
		<label>Nama</label>
		<input type="text" class="form-control" value="{{$cast->nama}}" name="nama" placeholder="Masukkan Nama Cast">
		@error('nama')
		<div class="alert alert-danger">
			{{ $message }}
		</div>
    	@enderror
    </div>
    <div class="form-group">
        <label>Umur</label>
        <input type="integer" class="form-control" value="{{$cast->umur}}" name="umur" iplaceholder="Masukkan Umur">
         @error('umur')
    	  <div class="alert alert-danger">
             {{ $message }}
          </div>
        @enderror
    </div>
	<div class="form-group">
        <label>Bio</label>
        <textarea name="bio" class="form-control" id="" cols="30" rows="10">{{$cast->bio}}</textarea>
         @error('bio')
    	  <div class="alert alert-danger">
             {{ $message }}
          </div>
        @enderror
    </div>
        <button type="submit" class="btn btn-primary">Update</button>
</form>        

	
@endsection