<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('halaman.register');
    }

    public function welcome(Request $request){
        // dd($request->all());
        $nama1 = $request->namaAwal;
        $nama2 = $request->namaAkhir;

        return view('halaman.welcome', compact('nama1','nama2'));
    }
}